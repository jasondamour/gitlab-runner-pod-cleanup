// +build integration

package app

import (
	"context"
	"fmt"
	"os/exec"
	"testing"
	"time"

	"github.com/jpillora/backoff"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/config"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/kubeclient"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/logging"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sClient "k8s.io/client-go/kubernetes"
)

type podDefinition struct {
	podName     string
	annotations map[string]string
	containerDefinition
}

type containerDefinition struct {
	containerName     string
	containerImage    string
	containerCommands []string
	containerArgs     []string
}

func skipIntegrationTests(t *testing.T, cmd ...string) {
	if testing.Short() {
		t.Skip("Skipping long tests")
	}

	if len(cmd) == 0 {
		return
	}

	executable, err := exec.LookPath(cmd[0])
	if err != nil {
		t.Skip(cmd[0], "doesn't exist", err)
	}

	if err := executeCommandSucceeded(executable, cmd[1:]); err != nil {
		assert.FailNow(t, "failed integration test command", "%q failed with error: %v", executable, err)
	}
}

// executeCommandSucceeded tests whether a particular command execution successfully
// completes. If it does not, it returns the error produced.
func executeCommandSucceeded(executable string, args []string) error {
	cmd := exec.Command(executable, args...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("%w - %s", err, string(out))
	}

	return nil
}

func TestCleanOnCluster(t *testing.T) {
	skipIntegrationTests(t, "kubectl", "cluster-info")

	client := getClient(t, "pod-cleanup-integration-test")

	defaultGetPodsManager := func(namespace, annotation string) *PodsManager {
		config := config.Config{
			LogLevel:             "debug",
			LogFormat:            logging.FormatJSON,
			Interval:             "5s",
			CacheCleanupInterval: "30m",
			CacheExpiration:      "0.5h",
			Limit:                3,
			MaxErrAllowed:        2,
			Kubernetes: config.KubernetesConfig{
				Namespaces:     []string{namespace},
				Annotation:     annotation,
				RequestTimeout: "10s",
				RequestLimit:   3,
			},
		}

		return createPodsManager(t, client, config, namespace)
	}

	containerDef := containerDefinition{
		containerName:     "sleep",
		containerImage:    "alpine:3.13",
		containerCommands: []string{"sleep"},
		containerArgs:     []string{"180"},
	}

	defaultPrepare := func(t *testing.T, client *k8sClient.Clientset, namespace string) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		k8sNamespace := &corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name:         namespace,
				GenerateName: namespace,
			},
		}
		k8sNamespace, err := client.CoreV1().Namespaces().Create(ctx, k8sNamespace, metav1.CreateOptions{})
		require.NotNil(t, k8sNamespace)
		require.NoError(t, err)
	}

	defaultFinalize := func(t *testing.T, client *k8sClient.Clientset, namespace string) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		pods, err := client.CoreV1().Pods(namespace).List(ctx, metav1.ListOptions{})
		require.NotNil(t, pods)
		assert.NoError(t, err)

		for _, pod := range pods.Items {
			err = client.CoreV1().Pods(namespace).Delete(ctx, pod.GetName(), metav1.DeleteOptions{})
			assert.NoError(t, err)
			time.Sleep(1 * time.Millisecond)
		}

		err = client.CoreV1().Namespaces().Delete(ctx, namespace, metav1.DeleteOptions{})
		require.NoError(t, err)
	}

	tests := map[string]struct {
		getPodsManager func(string, string) *PodsManager
		namespace      string
		annotation     string
		podDefinitions func(string) []podDefinition
		prepare        func(*testing.T, *k8sClient.Clientset, string)
		verify         func(*testing.T, *PodsManager, string, error)
		finalize       func(*testing.T, *k8sClient.Clientset, string)
	}{
		"cannot delete any pods": {
			getPodsManager: defaultGetPodsManager,
			namespace:      "ns-no-delete",
			annotation:     "an-no-delete",
			podDefinitions: func(annotation string) []podDefinition {
				return []podDefinition{
					{
						podName:             "pod-faulty-ttl",
						annotations:         map[string]string{annotation: "faulty"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-15m-ttl",
						annotations:         map[string]string{annotation: "15m"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-10-ttl",
						annotations:         map[string]string{annotation: "10"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-no-ttl-annotation",
						containerDefinition: containerDef,
					},
				}
			},
			prepare: defaultPrepare,
			verify: func(t *testing.T, pm *PodsManager, namespace string, err error) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
				defer cancel()

				assert.Error(t, err)

				pods, err := client.CoreV1().Pods(namespace).List(ctx, metav1.ListOptions{})
				assert.NoError(t, err)
				assert.Len(t, pods.Items, 4)
			},
			finalize: defaultFinalize,
		},
		"delete all pods": {
			getPodsManager: defaultGetPodsManager,
			namespace:      "ns-delete-all",
			annotation:     "an-delete-all",
			podDefinitions: func(annotation string) []podDefinition {
				return []podDefinition{
					{
						podName:             "pod-1s-1-ttl",
						annotations:         map[string]string{annotation: "1s"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-1s-2-ttl",
						annotations:         map[string]string{annotation: "1s"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-1s-3-ttl",
						annotations:         map[string]string{annotation: "1s"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-1s-4-ttl",
						annotations:         map[string]string{annotation: "1s"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-2s-ttl",
						annotations:         map[string]string{annotation: "2s"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-3s-ttl",
						annotations:         map[string]string{annotation: "3s"},
						containerDefinition: containerDef,
					},
				}
			},
			prepare: defaultPrepare,
			verify: func(t *testing.T, pm *PodsManager, namespace string, err error) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
				defer cancel()

				assert.Error(t, err)

				pods, err := client.CoreV1().Pods(namespace).List(ctx, metav1.ListOptions{})
				assert.NoError(t, err)
				assert.Len(t, pods.Items, 0)
			},
			finalize: defaultFinalize,
		},
		"delete all pods but one": {
			getPodsManager: defaultGetPodsManager,
			namespace:      "ns-delete-all-but-one",
			annotation:     "an-delete-all-but-one",
			podDefinitions: func(annotation string) []podDefinition {
				return []podDefinition{
					{
						podName:             "pod-1s-ttl",
						annotations:         map[string]string{annotation: "1s"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-2s-ttl",
						annotations:         map[string]string{annotation: "2s"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-15m-ttl",
						annotations:         map[string]string{annotation: "15m"},
						containerDefinition: containerDef,
					},
				}
			},
			prepare: defaultPrepare,
			verify: func(t *testing.T, pm *PodsManager, namespace string, err error) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
				defer cancel()

				assert.Error(t, err)

				pods, err := client.CoreV1().Pods(namespace).List(ctx, metav1.ListOptions{})
				assert.NoError(t, err)
				assert.Len(t, pods.Items, 1)
				assert.Contains(t, pods.Items[0].GetName(), "pod-15m-ttl")
			},
			finalize: defaultFinalize,
		},
		"delete three pods on three pages": {
			getPodsManager: defaultGetPodsManager,
			namespace:      "ns-delete-3-pods-on-3-pages",
			annotation:     "an-delete-3-pods-on-3-pages",
			podDefinitions: func(annotation string) []podDefinition {
				return []podDefinition{
					{
						podName:             "pod-0-ttl",
						annotations:         map[string]string{annotation: "1s"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-1-ttl",
						annotations:         map[string]string{annotation: "10m"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-2-ttl",
						annotations:         map[string]string{annotation: "15m"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-3-ttl",
						annotations:         map[string]string{annotation: "2s"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-4-ttl",
						annotations:         map[string]string{annotation: "10m"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-5-ttl",
						annotations:         map[string]string{annotation: "15m"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-6-ttl",
						annotations:         map[string]string{annotation: "3s"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-7-ttl",
						annotations:         map[string]string{annotation: "3s"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-8-ttl",
						annotations:         map[string]string{annotation: "15m"},
						containerDefinition: containerDef,
					},
					{
						podName:             "pod-9-ttl",
						annotations:         map[string]string{annotation: "15m"},
						containerDefinition: containerDef,
					},
				}
			},
			prepare: defaultPrepare,
			verify: func(t *testing.T, pm *PodsManager, namespace string, err error) {
				ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
				defer cancel()

				assert.Error(t, err)

				pods, err := client.CoreV1().Pods(namespace).List(ctx, metav1.ListOptions{})
				assert.NoError(t, err)
				assert.Len(t, pods.Items, 6)
			},
			finalize: defaultFinalize,
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			tc.prepare(t, client, tc.namespace)

			for _, podDef := range tc.podDefinitions(tc.annotation) {
				pod, err := createPod(client, tc.namespace, podDef)
				require.NotNil(t, pod)
				require.NoError(t, err)
				time.Sleep(1 * time.Millisecond)
			}

			go func() {
				time.Sleep(20 * time.Second)
				cancel()
			}()

			pm := tc.getPodsManager(tc.namespace, tc.annotation)
			err := pm.Clean(ctx)

			tc.verify(t, pm, tc.namespace, err)
			tc.finalize(t, client, tc.namespace)
		})
	}
}

func getClient(t *testing.T, userAgent string) *k8sClient.Clientset {
	client, err := kubeclient.GetKubeClient(userAgent)
	require.NoError(t, err)
	require.NotNil(t, client)

	return client
}

func createPod(
	client *k8sClient.Clientset,
	namespace string,
	definition podDefinition,
) (*corev1.Pod, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	var terminationGrace int64 = 0

	pod := &corev1.Pod{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Pod",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Namespace:   namespace,
			Name:        definition.podName,
			Annotations: definition.annotations,
		},
		Spec: corev1.PodSpec{
			RestartPolicy:                 corev1.RestartPolicyNever,
			TerminationGracePeriodSeconds: &terminationGrace,
			Containers: []corev1.Container{
				{
					Name:    definition.containerName,
					Image:   definition.containerImage,
					Command: definition.containerCommands,
					Args:    definition.containerArgs,
				},
			},
		},
	}

	return client.CoreV1().Pods(namespace).Create(ctx, pod, metav1.CreateOptions{})
}

func createPodsManager(
	t *testing.T,
	client *k8sClient.Clientset,
	config config.Config,
	namespace string,
) *PodsManager {
	manager, err := NewPodsManager(newPodsManagerOpts{
		cfg:        config,
		logger:     logging.New(),
		backoff:    &backoff.Backoff{Min: time.Second, Max: 30 * time.Second},
		kubeClient: client,
		namespace:  namespace,
	})

	require.NoError(t, err)
	return manager
}
